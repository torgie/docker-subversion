#!/bin/sh
set -e

svnserve --daemon --root /opt/svn --pid-file /var/run/svnserve.pid --log-file /var/log/svn.log

tail -f /var/log/svn.log
