## Docker subversion image

### To build the image

`docker-compose -f docker-compose.yml build`

### To run the image

`docker-compose -f docker-compose.yml start`

Don't forget to:

* Mount the SVN repo into **/opt/svn**
* Expose and publish port **3690**
